'''
To read the csv file
'''
import csv
import collections
import matplotlib.pyplot as plot
import helper

def calculate_the_economy(top_bowlers):
    '''
    Calculating the economy of bowlers
    '''
    bowlers_economy = {}
    for bowler in top_bowlers:
        bowlers_economy[bowler] = round(
            (top_bowlers[bowler]['conceded_runs']/top_bowlers[bowler]['balls'])*6, 2)
    return collections.OrderedDict(sorted(
        bowlers_economy.items(), key=lambda bowler_entry: bowler_entry[1], reverse=True))

def get_economy_of_bowlers(match_file, delivery_file, year):
    '''
    Getting economy of each bowler
    '''
    top_bowlers = {}
    match_id = helper.get_id_set(match_file, year)
    with open(delivery_file, 'r') as deliversfile:
        delivery_reader = csv.DictReader(deliversfile)
        for match in delivery_reader:
            if match['match_id'] in match_id:
                wide = int(match['wide_runs'])
                no_ball = int(match['noball_runs'])
                delivery_bowler = match['bowler']
                total_run = int(match['total_runs'])
                bye_run = int(match['bye_runs'])
                legbye_runs = int(match['legbye_runs'])
                conceded_run = total_run-bye_run-legbye_runs
                if delivery_bowler not in top_bowlers:
                    top_bowlers[delivery_bowler] = {}
                    top_bowlers[delivery_bowler]['conceded_runs'] = 0
                    top_bowlers[delivery_bowler]['balls'] = 0
                top_bowlers[delivery_bowler]['conceded_runs'] += conceded_run
                if wide == 0 and no_ball == 0:
                    top_bowlers[delivery_bowler]['balls'] += 1
    bowlers_economy = calculate_the_economy(top_bowlers)
    print(bowlers_economy)

def plot_economy_vs_bowler(bowlers_economy):
    '''
    Plotting bowlers with low economy
    '''
    team = []
    conceded_runs = []
    bowlers_economy = sorted(bowlers_economy.items(), key=lambda x: x[1])
    for bowler in bowlers_economy:
        team.append(bowler[0])
        conceded_runs.append(bowler[1])
    team = team[:20]
    conceded_runs = conceded_runs[:20]
    plot.barh(team, conceded_runs)
    plot.title("Top 20 Bowlers VS Economy")
    plot.xlabel('Economy')
    plot.ylabel('Bowlers')
    plot.show()

plot_economy_vs_bowler(get_economy_of_bowlers('matches.csv', 'deliveries.csv', 2015))
