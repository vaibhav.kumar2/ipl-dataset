"""To read the csv file"""
import csv
import matplotlib.pyplot as plot
import helper


def extra_runs_conceded(match_file, delivery_file, year):
    '''
    Calculating the extra runs conceded by team
    '''
    teams = {}
    match_id = helper.get_id_set(match_file, year)
    with open(delivery_file, 'r') as deliveryfile:
        delivery_reader = csv.DictReader(deliveryfile)
        for match in delivery_reader:
            if match['match_id'] in match_id:
                bowling_teams = match['bowling_team']
                extra_run = match['extra_runs']
                if bowling_teams not in teams:
                    teams[bowling_teams] = 0
                teams[bowling_teams] += int(extra_run)
    return teams

def plot_conceded_runs_vs_team(teams):
    '''
    Plotting the Conceded runs and the team
    '''
    team = []
    conceded_runs = []
    teams = sorted(teams.items(), key=lambda x: x[1])
    for match in teams:
        team.append(match[0])
        conceded_runs.append(match[1])
    plot.figure(figsize=(12, 6))
    plot.barh(team, conceded_runs)
    plot.title("Team VS Conceded Runs in 2016")
    plot.xlabel('Conceded Runs')
    plot.ylabel('Team')
    plot.tight_layout()
    plot.show()

plot_conceded_runs_vs_team(extra_runs_conceded('matches.csv', 'deliveries.csv', 2016))
