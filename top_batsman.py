'''
Read the CSV file
'''
import csv
import matplotlib.pyplot as plot
import helper

def get_top_batsman(match_file, delivery_file, year):
    '''
    Getting the top-batsman
    '''
    top_batsman = {}
    match_id = helper.get_id_set(match_file, year)
    with open(delivery_file, 'r') as deliveryfile:
        deliveries_reader = csv.DictReader(deliveryfile)
        for match in deliveries_reader:
            if match['match_id'] in match_id:
                super_over = bool(match['is_super_over'])
                batsman_run = int(match['batsman_runs'])
                striker = match['batsman']
                if super_over:
                    if striker not in top_batsman:
                        top_batsman[striker] = 0
                    top_batsman[striker] += batsman_run
    return top_batsman

def plot_batsman_vs_runs(top_batsman):
    '''
    Plotting the top batsman and his runs
    '''
    batsman = []
    batsman_runs = []
    top_batsman = sorted(top_batsman.items(), key=lambda x: x[1])
    for batter in top_batsman:
        batsman.append(batter[0])
        batsman_runs.append(batter[1])
    batsman = batsman[-20:]
    batsman_runs = batsman_runs[-20:]
    plot.barh(batsman, batsman_runs)
    plot.title("Top 20 Batsman vs score in 2015")
    plot.xlabel('Runs')
    plot.ylabel('Batsman')
    plot.show()

plot_batsman_vs_runs(get_top_batsman('matches.csv', 'deliveries.csv', 2015))
