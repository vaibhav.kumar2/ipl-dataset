Name : IPL DATASET PROJECT.

Question:
In this data assignment you will transform raw data from IPL into graphs that will convey some meaning / analysis. For each part of this assignment you will have 2 parts -
Dataset: Download both csv files from https://www.kaggle.com/manasgarg/ipl
Code python functions that will transform the raw csv data into a data structure in a format suitable for plotting with matplotlib.

Generate the following plots ...
1. Plot the number of matches played per year of all the years in IPL.
2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.
3. For the year 2016 plot the extra runs conceded per team.
4. For the year 2015 plot the top economical bowlers.
5. Discuss a "Story" you want to tell with the given data. As with part 1, prepare the data structure and plot with     matplotlib.

Modules:

1. matches_played_per_year.py
2. team_won_ipl.py
3. conceded_runs_by_team.py
4. bowlers_economy.py
5. top_batsman.py
6. helper.py

.  Specification: Python 3.7
.  IDE: VS Code
.  For Plotting: Matplotlib
.  For Naming: Used Snake-Case