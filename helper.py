import csv
def get_id_set(match_file, year):
    match_id = set()
    with open(match_file, 'r') as matchfile:
        matches_reader = csv.DictReader(matchfile, delimiter=',')
        for match in matches_reader:
            season = match['season']
            if int(season) == year:
                match_id.add(match['id'])
    return match_id
