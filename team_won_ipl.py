'''
To read the CSV file
'''
import csv
import matplotlib.pyplot as plot


def find_seasons(filename):
    '''
    Getting all the seasons
    '''
    seasons = set()
    with open(filename) as matchesfile:
        matches_reader = csv.DictReader(matchesfile)
        for match in matches_reader:
            seasons.add(match['season'])
    seasons = list(seasons)
    seasons.sort()
    return seasons

def matches_won_by_team(match_file):
    '''
    Matches won by team  per year
    '''
    matches = {}
    season_set = find_seasons(match_file)
    with open(match_file, 'r') as matchfile:
        matches_reader = csv.DictReader(matchfile, delimiter=',')
        for match in matches_reader:
            match_winner_team = match['winner']
            seasons = match['season']
            if match['winner'] == "":
                continue
            if match_winner_team not in matches:
                matches[match_winner_team] = {}
            for current_year in season_set:
                if current_year not in matches[match_winner_team]:
                    matches[match_winner_team][current_year] = 0
            matches[match_winner_team][seasons] += 1
    return matches

def plot_match_win_vs_season(matches):
    '''
    Plotting match win by team per year
    '''
    extra_counts = []
    team_wins = []
    for team in matches:
        all_teams = list(matches)
        season_for_each_match = list(matches[team])
        for count_matches in matches[team]:
            extra_counts.append(matches[team][count_matches])
        team_wins.append(extra_counts.copy())
        extra_counts.clear()
    season_for_each_match.sort()
    bottom_height = [0]*10
    for win_count in team_wins:
        match_label = all_teams[team_wins.index(win_count)]
        plot.bar(season_for_each_match, win_count, bottom=bottom_height, label=match_label)
        bottom_height = [x+y for (x, y) in zip(win_count, bottom_height)]
    plot.xlabel('Season')
    plot.ylabel('Match Win By Team')
    plot.legend(bbox_to_anchor=(1.06, 0.98), ncol=5, loc=4, borderaxespad=1)
    plot.show()

plot_match_win_vs_season(matches_won_by_team('matches.csv'))
