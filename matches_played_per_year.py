'''
To read the CSV file
'''
import csv
import matplotlib.pyplot as plot

def get_matches_per_year(file):
    '''
    Getting matches per year played by team
    '''
    result = {}
    with open(file, 'r') as matchfile:
        matches_reader = csv.DictReader(matchfile, delimiter=',')
        for match in matches_reader:
            year = match['season']
            if year not in result:
                result[year] = 1
            else:
                result[year] += 1
    return result

def plot_match_vs_year(result):
    '''
    Plotting the match played per year
    '''
    seasons = []
    match_played_per_year = []
    for season in sorted(result):
        seasons.append(season)
        match_played_per_year.append(result[season])
    plot.bar(seasons, match_played_per_year)
    plot.title("Matches VS Season")
    plot.xlabel('Season')
    plot.ylabel('Matches')
    plot.show()

plot_match_vs_year(get_matches_per_year('matches.csv'))
